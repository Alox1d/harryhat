package com.alox1d.harrypotter.ui.scenes.hat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.alox1d.harrypotter.R

class HatActivity : AppCompatActivity() {
    private lateinit var hatViewModel: HatViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hat)
        hatViewModel = ViewModelProviders.of(this).get(HatViewModel::class.java)
    }
}