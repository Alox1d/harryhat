package com.alox1d.harrypotter.ui.scenes.hat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alox1d.harrypotter.domain.repositories.IHatRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HatViewModel : ViewModel() {
    private lateinit var  hatRepository:IHatRepository
    private val _isLoading:MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value=false }
    private val _facultyName:MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    val facultyName:LiveData<String> = _facultyName
    val isLoading : LiveData<Boolean> = _isLoading
    fun getFacultyName(name:String, surname:String){
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.IO){
                _facultyName.postValue(hatRepository.generateFaculty(name,surname).name)
                _isLoading.postValue(false)

            }
        }
    }
}
