package com.alox1d.harrypotter.domain.repositories

import com.alox1d.harrypotter.domain.models.FacultyModel

interface IHatRepository {
    suspend fun generateFaculty(name:String, surname:String):FacultyModel // остснавливаемая (асигхронная) функция
}