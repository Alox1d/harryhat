package com.alox1d.harrypotter.domain.repositories

import com.alox1d.harrypotter.domain.models.FacultyModel
import kotlinx.coroutines.delay

class HotRepository:IHatRepository {
    override suspend fun generateFaculty(name: String, surname: String):FacultyModel {
        delay(5000)
        //withContext(Dispatchers.Main    )
        return if (name == "Harry" && surname == "Potter") {
            FacultyModel("Griffindor")
        } else {
            FacultyModel("Slytherin")
        }
    }
}